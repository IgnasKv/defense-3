﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ScholarShips
{
    class Student
    {
        public string surname { get; set; }
        public string name { get; set; }
        public string group { get; set; }
        public int nofMarks { get; set; }
        public double scholarship { get; set; }
        public string typeOfscholarship { get; set; }
        public ArrayList marks { get; set; }

        public Student()
        {
            surname = "";
            name = "";
            group = "";
            nofMarks = 0;
            typeOfscholarship = "none";
            scholarship = 0;
            marks = new ArrayList();
        }

        public Student(string surname, string name, string group, int nofMarks, ArrayList marks, string type)
        {
            this.surname = surname;
            this.name = name;
            this.group = group;
            this.nofMarks = nofMarks;
            typeOfscholarship = type;
            scholarship = 0;
            this.marks = new ArrayList();
            foreach (int i in marks)
            {
                this.marks.Add(i);
            }
        }

        public override string ToString()
        {
            string line = string.Format("{0,15}{1,15}{2,8}{3,9}{4,14}|", surname, name, group, nofMarks, Math.Round(scholarship, 2));
            foreach (int i in marks)
            {
                line = line + string.Format("{0,3}", i);
            }
            return line;
        }

        public static bool operator <=(Student st1, Student st2) 
        {
            int ip = String.Compare(st1.surname, st2.surname,
           StringComparison.CurrentCulture);
            int iv = String.Compare(st1.name, st2.name,
           StringComparison.CurrentCulture);
            return (ip < 0 || ip == 0 && iv < 0);
        }

        public static bool operator >=(Student st1, Student st2)
        {
            int ip = String.Compare(st1.surname, st2.surname,
           StringComparison.CurrentCulture);
            int iv = String.Compare(st1.name, st2.name,
           StringComparison.CurrentCulture);
            return !(ip < 0 || ip == 0 && iv < 0);
        }

    }
}
