﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScholarShips
{
    class Students
    {
        const int CMax = 100;
        private Student[] St;
        private int n;
        public int nrml { get; set; } //Tracks number of students who get normal scholarship
        public int adv { get; set; } //Tracks number of students who get advanced scholarship

        public Students()
        {
            St = new Student[CMax];
            n = 0;
            nrml = 0;
            adv = 0;
        }

        public Student GetStudent(int index)
        {
            return St[index];
        }

        public int Get()
        {
            return n;
        }

        // Creates a new land
        public void Set(Student s)
        {
            St[n++] = s;
            if (s.typeOfscholarship == "nrml")
            {
                nrml++;
            }
            else if (s.typeOfscholarship == "adv")
            {
                adv++;
            }
        }

        public void SortABC() //Sort according to surnames and names
        {
            for (int i = 0; i < n-1; i++)
            {
                int minPos = i;
                for (int j = i + 1; j < n; j++)
                {
                    if (St[j] <= St[minPos])
                    {
                        minPos = j;
                    }                    
                }
                Student temp = St[minPos];
                St[minPos] = St[i];
                St[i] = temp;
            }
        }

        public void SortScholarships() //Sort according to scholarships
        {
            for (int i = 0; i < n-1; i++)
            {
                int minPos = i;
                for (int j = i + 1; j < n; j++)
                {
                    if (St[j].scholarship > St[minPos].scholarship)
                    {
                        minPos = j;
                    }
                }
                Student temp = St[minPos];
                St[minPos] = St[i];
                St[i] = temp;
            }
        }

        public void Remove()
        {
            int i = 0;
            while (i < n)
            {
                if (St[i].typeOfscholarship == "none")
                {
                    for (int j = i; j < n - 1; j++)
                    {
                        St[j] = St[j + 1];
                    }
                    n--;
                }
                else
                {
                    i++;
                }
            }
        }        

        public override string ToString()
        {
            const string separation = "\n-------------------------------------------------------------|---------------";

            string line = string.Format(separation);
            line = line + string.Format("\n{0,15}{1,15}{2,8}{3,9}{4,14}|{5,10}", "Surname", "Name", "Group", "Number", "Schollarship", "Marks");
            line = line + string.Format(separation);
            for (int i = 0; i < n; i++)
            {
                line = line + string.Format("\n{0}", St[i].ToString());
            }
            line = line + string.Format(separation);
            return line;
        }
    }
}
