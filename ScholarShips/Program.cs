﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace ScholarShips
{
    class Program
    {
        static void Main(string[] args)
        {
            const string data = "Data.txt";            
            Students students = new Students();
            Students advStudents = new Students();
            string advGroup;
            double fund;
            double average;
            Read(data, students, out fund, out average);
            SetScholarShipAmount(students, fund);

            Console.WriteLine("The scholarship fund is: {0}\nThe average mark to get a scholarship should be atleast: {1}", fund, average);
            students.SortABC();
            students.SortScholarships();
            Console.WriteLine(students.ToString());

            students.Remove();
            Console.WriteLine(students.ToString());

            Console.WriteLine("Search advanced students by group: ");
            advGroup = Console.ReadLine();
            Create(students, advStudents, advGroup);
            Console.WriteLine(advStudents.ToString());
        }

        private static void Read(string data, Students s, out double fund, out double ave)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                string line;
                line = sr.ReadLine();

                string[] parts = line.Split(' ');
                fund = double.Parse(parts[0]);
                ave = double.Parse(parts[1]);
                ArrayList marks = new ArrayList();

                while ((line = sr.ReadLine()) != null)
                {
                    parts = line.Split(' ');

                    string surname = parts[0];
                    string name = parts[1];
                    string group = parts[2];
                    int n = int.Parse(parts[3]);
                    string type;
                    marks.Clear();

                    for (int i = 0; i < n; i++)
                    {
                        marks.Add(int.Parse(parts[4+i]));
                    }

                    type = ScolarshipType(marks, n, ave);

                    Student student = new Student(surname, name, group, n, marks, type);

                    s.Set(student);
                }

            }
        }

        private static void Create(Students original, Students adv, string group)
        {
            for (int i = 0; i < original.Get(); i++)
            {
                if (original.GetStudent(i).typeOfscholarship == "adv" && original.GetStudent(i).group == group)
                {
                    adv.Set(original.GetStudent(i));
                }
            }
        }

        private static string ScolarshipType(ArrayList marks, int nofMarks, double mainAve) //Checks what type of scholarship does a student get (none, normal or advanced)
        {
            int sum = 0;
            double ave;
            string type = "none";
            bool advanced = false;

            foreach (int i in marks) // Calculates students mark sum and instantly checks if no mark is lower than 4
            {
                if (i > 4)
                {
                    sum = sum + i;
                }
                else
                {
                    return type;
                }
            }

            ave = sum / nofMarks;

            foreach (int i in marks) // Checks if every mark is higher than 8
            {
                if (i > 8)
                {
                    advanced = true;
                }
                else
                {
                    advanced = false;
                    break;
                }
            }            

            if ( ave > mainAve)
            {
                type = "nrml";
                if (advanced)
                {
                    type = "adv";
                }
            }

            return type;
        }

        private static void SetScholarShipAmount(Students s, double amount) //Calculates the amount of scholarship and assigns it to the students
        {
            int nrml = s.nrml;
            int adv = s.adv;
            double scholarship = amount / (nrml + adv * 1.1);

            for (int i = 0; i < s.Get(); i++)
            {
                if (s.GetStudent(i).typeOfscholarship == "nrml")
                {
                    s.GetStudent(i).scholarship = scholarship;
                }
                else if (s.GetStudent(i).typeOfscholarship == "adv")
                {
                    s.GetStudent(i).scholarship = scholarship * 1.1;
                }
            }
        }
    }
}
